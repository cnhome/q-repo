===========
Description
===========

The first step to develop python command line tool was to get and idea about the 'contents file'.
It turns out that it is compressed file stored on the web.
To view and understand the structure of the file, the contents file needed to be downloaded and decompressed.
The contents file has data written into columns separated by empty spaces. The first column represents file stored in a package and the last column represents the package name. If the package contains number of files the package name appears multiple times in the content file in a separate line.
Count number of files belonging to the package is therefore equal to number of occurrences of the package name. 

=======
Aproach
=======

- Download 'contents file'.
- Read 'contents file' and count number of occurrences of the package names.
- Sort package names based on number of occurrences in 'contents file' in descending order.
- Show the result.

Download file
=============

In order to download the contents file two arguments are needed. A url of a repository and the architecture. The default url is read from a configuration file in 'etc/q-repo/q-repo.yaml'. The architecture is the parameter that needs to be submitted by user. The file is downloaded on disk in location specified with configuration parameter given in 'etc/q-repo/q-repo.yaml'.

Read contents file
==================

After the contents file is downloaded it can be read and analysed line by line. Package names and count of the package occurrences are stored in dictionary. Dictionary is used because it can be easily sorted by value. Format of the item in dictionary is <package_name>: <count_of_package_occurrences>.  

Sort package names
==================

Storing data in a dictionary gives an advantage that data can be easily sorted. In this case the sort is in descending order by value. 

Show result
===========

To display data in the console separate function is used. It takes dictionary of the packages and shows it in formatted output.

Structure of the project
========================

    q_repo
    ├── etc
    │   └── q-repo
    │       └── q-repo.yaml
    ├── q_repo
    │   ├── analyser
    │   │   └── stats.py
    │   ├── console
    │   │   └── cli.py
    │   └── utils
    │       ├── handler.py
    ├── README.rst
    ├── Description.rst
    ├── requirements.txt
    ├── setup.cfg
    └── setup.py
