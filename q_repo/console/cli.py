# -*- coding: utf-8 -*-
"""
Module to analyse a 'Contents index' file of a repository.
"""
import click

from q_repo import CONF
from q_repo.analyser.stats import get_package_stats


@click.group()
def q_repo():
    """
    q-repo command line utility to analyse 'Content index' file of the
    repository.
    """


@q_repo.command()
@click.option('--repo_url', default=CONF.repository_url,
              help='Repo url. (%s)' % CONF.repository_url)
@click.option('--arch', default=None,
              help='Architecture (e.g. amd64)')
@click.option('--no_results', default=CONF.no_results,
              help='Number of results to be displayed.')
def package_statistics(repo_url, arch, no_results):
    """
    Gets package statics based on repository url and the architecture.

    \b
    REPO_URL        url of the repository.
    ARCH            architecture (e.g. amd64)
    NO_RESULTS      number of results to be displayed

    \b
    Usage:
        $ q-repo package-statistics --arch amd64
        $ q-repo package-statistics --arch amd64 --no_results 15
        $ q-repo package-statistics --repo_url http://ftp.uk.debian.org/debian/dists/stable/main/
                                    --arch amd64
                                    --no_results 15
    """
    if arch:
        get_package_stats(repo_url, arch, no_results)
    else:
        print("Please enter --arch option. (e.g. amd64, arm64,...)")
