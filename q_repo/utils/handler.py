"""
Helper functiions to download and parse 'Content index' file.
"""

import os
import gzip
import wget


def download_package_file(url, gz_file):
    """
    Downloads contents index file and saves it to a disk.
    """
    if os.path.exists(gz_file):
        os.remove(gz_file)
    wget.download(url, gz_file)


def get_package_from_line(line):
    """
    Gets package name from the line read from the contents index file.
    The name of the package is the last item in the list.
    """
    package = line.split()[-1]
    return package


def get_packages_dict(file_path):
    """
    Reads the contents index file and gets package names. It returns
    dictionary of package names and sum of the files contained in the package.
    """
    packages = {}
    with gzip.open(file_path, 'rb') as file:
        line = file.readline()
        while line:
            line = line.decode().strip()
            package = get_package_from_line(line)
            if package not in packages:
                packages[package] = 1
            else:
                packages[package] = packages[package] + 1
            line = file.readline()
    return packages


def sort_dict(pkgs_dict):
    """
    Sorts dictionary on values.
    """
    return sorted(pkgs_dict.items(), key=lambda x: x[1], reverse=True)


def print_packages(sorted_packages, no_results):
    """
    Formats and prints the output for given number of results to be displayed.
    """
    i = 0
    print("\n")
    print("{0:5}   {1:70} {2}".format("   No", "Package", "Count"))
    print("-" * 90)
    while i < no_results:
        print("{0:5}.   {1:70} {2}".format(i + 1, sorted_packages[i][0], sorted_packages[i][1]))
        i = i + 1
