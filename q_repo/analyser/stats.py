"""
Package to analyse contents file of a repository.
"""
import os
from q_repo import CONF

from q_repo.utils.handler import download_package_file, get_packages_dict, sort_dict, print_packages


def get_package_stats(repo_url, arch, no_results):
    """
    Gets package statics based on repository url and the architecture.
    """
    url = repo_url + 'Contents-%s.gz' % arch
    gz_file = CONF.download_dir + 'Contents-%s.gz' % arch
    if not os.path.exists(CONF.download_dir):
        os.makedirs(CONF.download_dir)
    download_package_file(url, gz_file)
    packages = get_packages_dict(gz_file)
    sorted_packages = sort_dict(packages)
    print_packages(sorted_packages, no_results)
