*********************
Welcome to 'q-repo'
*********************

======================
Install prerequissites
======================

    $ sudo apt install python3-pip

    $ sudo pip3 install virtualenv

========================================
Create and activate virtual environment
========================================

    $ virtualenv -p python3 q-repo-env

    $ . q-repo-env/bin/activate

===================
Install the project
===================
    
    $ cd ~

    $ git clone git@gitlab.com:cnhome/q-repo.git

    $ cd q-repo

    $ pip3 install -r requirements.txt

    $ python setup.py develop

=====
Usage
=====

    $ q-repo --help
